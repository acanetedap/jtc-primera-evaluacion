package socket;

import java.net.Socket;

import utils.LogUtil;

public class ConversorRomanoThread extends Thread {
    
	//Referencia al cliente que sera atendido
    private Socket cliente;
    
    public ConversorRomanoThread(Socket cliente) {
        this.cliente = cliente;
    }
    
    @Override
    public void run() {
    	LogUtil.INFO("Nueva peticion recibida...");
        //TODO: Pendiente de implementacion de logica que parsea peticion, procesa request y almacena resultado en la bd
    	
    	LogUtil.INFO("Finalizando atencion de la peticion recibida...");
    }
    
}
